<!-- Jquery -->
<script type="text/javascript" src="../../../public/js/jquery.min.js"></script>
<!-- Bootstrap core Javascript -->
<script type="text/javascript" src="../../../public/bootstrap/js/bootstrap.min.js"></script>
<!-- Plugins -->
<script type="text/javascript" src="../../../public/js/jquery.easing.min.js"></script>
<script type="text/javascript" src="../../../public/js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="../../../public/js/scrollreveal.min.js"></script>
<script type="text/javascript" src="../../../public/js/script.js"></script>
</body>
</html>