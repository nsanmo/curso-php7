<?php

namespace cursophp7\app\exceptions;

use Exception;

class QueryException extends AppException
{
public function __construct(string $message, $code = 500)
{
    parent::__construct($message, $code);
}
}