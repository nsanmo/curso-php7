<?php

namespace cursophp7\app\exceptions;

use cursophp7\core\App;
use Exception;

class NotFoundException extends AppException
{
    public function __construct(string $message, $code = 404)
    {
        parent::__construct($message, $code);
    }
}