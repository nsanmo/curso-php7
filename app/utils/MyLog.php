<?php

namespace cursophp7\app\utils;

use Exception;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class MyLog
{
    private $log;

    private $level;

    /**
     * @param string $filename
     */
    private function __construct(string $filename, int $level)
    {
        $this->level = $level;
        $this->log = new Logger('name');
        $this->log->pushHandler(
            new StreamHandler($filename, $this->level)
        );
    }

    /**
     * @param string $filename
     * @return MyLog
     */
    public static function load(string $filename, int $level = Logger::INFO)
    {
        return new MyLog($filename, $level);
    }

    /**
     * @param string $message
     * @return void
     */
    public function add(string $message)
    {
        $this->log->log($this->level, $message);
    }
}