<?php

namespace cursophp7\app\repository;
use cursophp7\core\database\QueryBuilder;
use cursophp7\app\entity\Mensaje;

class MensajeRepository extends QueryBuilder
{
    /**
     * MensajeRepository constructor.
     */
    public function __construct(string $table = 'mensajes', string $classEntity = Mensaje::class)
    {
        parent::__construct($table, $classEntity);
    }
}