<?php

namespace cursophp7\app\controllers;

use cursophp7\app\entity\Asociado;
use cursophp7\app\exceptions\FileException;
use cursophp7\app\exceptions\ValidationException;
use cursophp7\app\repository\AsociadoRepository;
use cursophp7\app\utils\File;
use cursophp7\core\App;
use cursophp7\core\Response;

class AsociadoController
{

    /**
     * @return void
     */
    public function index()
    {
        $asociados = App::getRepository(AsociadoRepository::class)->findAll();
        Response::renderView('asociados', 'layout', compact('asociados'));
    }

    public function nuevo()
    {
        try {
            $nombre = trim(htmlspecialchars($_POST['nombre']));
            if (empty($nombre))
                throw new ValidationException('El nombre no puede quedar vacío');

            $descripcion = trim(htmlspecialchars($_POST['descripcion']));

            $tiposAceptados = ['image/jpeg', 'image/png', 'image/gif'];
            $imagenFile = new File('logo', $tiposAceptados);

            $imagenFile->saveUploadFile(Asociado::RUTA_IMAGENES_ASOCIADOS);
            $asociado = new Asociado($nombre, $imagenFile->getFileName(), $descripcion);

            App::getRepository(AsociadoRepository::class)->save($asociado);

            $message = "Se ha guardado un nuevo asociado: " . $asociado->getNombre();
            App::get('logger')->add($message);
        } catch (FileException $fileException)
        {
            die($fileException->getMessage());
        }
        App::get('router')->redirect('asociados');
    }
}