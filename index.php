<?php

use cursophp7\app\exceptions\AppException;
use cursophp7\app\exceptions\NotFoundException;
use cursophp7\core\App;
use cursophp7\core\Request;

try {
    require 'core/bootstrap.php';

    App::get('router')->direct(Request::uri(), Request::method());
}
catch (AppException $appException){
    $appException->handleError();
}