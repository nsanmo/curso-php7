<?php

namespace cursophp7\core;

class Security
{
    /**
     * @param string $role
     * @return bool
     * @throws \cursophp7\app\exceptions\AppException
     */
    public static function isUserGranted(string $role): bool
    {
        if ($role == 'ROLE_ANONYMOUS')
            return true;

        $usuario = App::get('appUser');
        if (is_null($usuario))
            return false;

        $valor_role = App::get('config')['security']['roles'][$role];
        $valor_role_usuario = App::get('config')['security']['roles'][$usuario->getRole()];

        return ($valor_role_usuario>=$valor_role);
    }

    /**
     * @param string $password
     * @return string
     */
    public static function encrypt(string $password): string
    {
        return password_hash($password, PASSWORD_BCRYPT);
    }

    /**
     * @param string $password
     * @param string $bd_password
     * @return bool
     */
    public static function checkPassword(string $password, string $bd_password): bool
    {
        return password_verify($password, $bd_password);
    }
}