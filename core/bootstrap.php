<?php

use cursophp7\app\repository\UsuarioRepository;
use cursophp7\app\utils\MyLog;
use cursophp7\core\App;
use cursophp7\core\Router;

session_start();

require __DIR__ . '/../vendor/autoload.php';

$config = require_once __DIR__ . '/../app/config.php';
App::bind('config', $config);

$router = Router::load('/home/php/proyectos/curso-php7.local/app/' . $config['routes']['filename']);
App::bind('router', $router);

$logger = MyLog::load('/home/php/proyectos/curso-php7.local/logs/' . $config['logs']['filename'], $config['logs']['level']);
App::bind('logger', $logger);

if (isset($_SESSION['loguedUser']))
    $appUser = App::getRepository(UsuarioRepository::class)->find($_SESSION['loguedUser']);
else
    $appUser = null;

App::bind('appUser',$appUser);