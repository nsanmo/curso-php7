<?php

namespace cursophp7\core;

class Response
{
    public static function renderView(string $name, string $layout='layout', array $data = [])
    {
        extract($data);

        $app['user'] = App::get('appUser');

        ob_start();
        require "/home/php/proyectos/curso-php7.local/app/views/$name.view.php";

        $mainContent = ob_get_clean();

        require "/home/php/proyectos/curso-php7.local/app/views/$layout.view.php";
    }
}